/*
	SELECT maker, 
	[pc], [laptop], [printer] -- �������� �� �������, ������� ������ � ����������� type, 
	-- ����������� ��������� �������� 
	FROM Product -- ����� ����� ���� ���������
	PIVOT -- ������������ �����-�������
	(COUNT(model) -- ���������� �������, ����������� ���������� ������� �������
	FOR type -- ����������� �������, 
	-- ���������� �������� � ������� ����� �������� ����������� ��������
	IN([pc], [laptop], [printer]) --����������� ���������� �������� � ������� type, 
	 -- ������� ������� ������������ � �������� ����������, 
	 -- �.�. ��� ����� ������������� �� ���
	) pvt ;-- ����� ��� ������� �������
*/
select dt, DTName, [3],[6]
from
(
	select dt,DTName,format(e,'0') c,IsNull(SEDId,0)sed from (
			select DTId dt,DTName,EID e from DocType full join Executor on 1=1 where EId in (3,6)
	)q
	left join 
	(select * from SpecExecDoc where SEDSpec=4)q2
	on SEDDocType=dt and SEDExec=e
)pt
pivot
(
	sum (sed) for c in ([3], [6])
)pvt
order by dt--,e