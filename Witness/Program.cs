﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Witness
{
  static class Program
  {
    const int error_timeout = 30000; // полминуты на ошибку сети и т.п.
    const int dontshoot_timeout = 300000; // раз в 5 минут для тех, кого не следим

    //\\192.168.204.9\foto_screen$

    //private static string folder= "D:\\~project\\94. СМУ 24\\материалы\\Скрины\\";
    private static int timeout = 3000;
    //private static DateTime ts = DateTime.Now;
    //static string tmp_folder = System.IO.Path.GetTempPath();
    //const string MyServerIp = "192.168.204.9"; // куда класть картинки

    public static bool IsDebugComputer() { return Environment.MachineName == "DESKTOP-ESR8QOJ"; }
    private const string ConnectionString_Debug = "Server=DESKTOP-ESR8QOJ;Database=SmuOk;Trusted_Connection=True;";
    private const string ConnectionString_Prod = "Server=SERVER-SMUOK\\SQLEXPRESS01;Database=SmuOk;Trusted_Connection=Yes;";
    public static string ConStr { get; } = IsDebugComputer() ? ConnectionString_Debug : ConnectionString_Prod;

    [STAThread]
    static void Main()
    {
      while (true) {
        try
        {
          //msg("get timeout from DB");
          timeout = int.Parse(MyGetOneValue("select EUScreenshot from _engUser where EULogin='" + Environment.UserDomainName + "\\" + Environment.UserName + "'").ToString());
          //msg("timeout = " + timeout.ToString());
          if (timeout > 0) MyScreenshot();
          else timeout = dontshoot_timeout;
        }
        catch(Exception)
        {
          //msg("error: " + ex.Message);
          timeout = error_timeout; // если сервер недоступен или доступа нет, или _engUser не заполнили — проверить через 30 секунд
        }        
        System.Threading.Thread.Sleep(timeout);
      }
    }

    //public static void msg(string s)
    //{
    //  System.Windows.Forms.MessageBox.Show(s);
    //}

    public static string MyES(object val, bool bForLike = false, bool clear_as_null = false)
    {
      string s = val.ToString();
      if (val.GetType().ToString() == "System.Decimal") s = s.Replace(',', '.');
      //заменяем пробел на % для LIKE
      s = s.Replace("\r", " ");//.Replace("\n", " ");
      while (s.Contains(" \n") || s.Contains("\n ") || s.Contains("\n\n") || s.Contains("  ")) s = s.Replace(" \n", "\n").Replace("\n ", "\n").Replace("\n\n", "\n").Replace("  ", " ");
      s = s.Replace(" .", ".").Replace(" ,", ",");
      s = s.Trim(new char[] { ' ', '\n' });
      if (clear_as_null && s == "" & bForLike == false)
        return "null";
      if (bForLike)
        return "N'%" + s.Replace("'", "''").Replace(" ", "%") + "%'";
      else
        return "N'" + s.Replace("'", "''") + "'";
    }

    static void MyExecute(string sQuery)
    {
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand Com = new SqlCommand(sQuery, con))
          {
            Com.ExecuteNonQuery();
          }
        }
      }
      catch (System.Exception ex)
      {
        //
      }
    }

    public static object MyGetOneValue(string sQuery)
    {
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand Com = new SqlCommand(sQuery, con))
          {
            Com.CommandTimeout = 10;
            using (SqlDataReader r = Com.ExecuteReader())
            {
              if (r.HasRows)
              {
                r.Read();
                return (r[0].ToString());
              }
              else
              {
                return ("");
              }
            }
          }
        }
      }
      catch (System.Exception ex)
      {
        //MyConsoleWriteErrorLine(ex.Message);
        return ("");
      }
    }


    static void MyScreenshot()
    {
      string s = Environment.UserDomainName + "\\" + Environment.UserName;
      s = s.Replace('/', '_').Replace('\\', '_');
      string SaveFilesTo = (string)MyGetOneValue("select EOValue from _engOptions where EOName='ScreensFolder'");
      if (!SaveFilesTo.EndsWith("\\")) SaveFilesTo += "\\";
      SaveFilesTo += s;
      if (!Directory.Exists(SaveFilesTo))
      {
        //System.Windows.Forms.MessageBox.Show("create folder: " + SaveFilesTo);
        Directory.CreateDirectory(SaveFilesTo);
      }
      SaveFilesTo += "\\";
      //string = "";
      //SaveFilesTo = "c:\\" + DateTime.Now.ToString("yyyy.MM.dd hh.mm.ss") + ".png";
      MyMakeScreenshot(SaveFilesTo);
    }

    private static Size GetTrueScreenSize(Screen screen)
    {
      //get system DPI
      var systemDPI = (int)Registry.GetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "LogPixels", 96);

      using (var graphics = Graphics.FromHwnd(IntPtr.Zero))
      {
        //get graphics DPI
        var dpiX = graphics.DpiX;
        var dpiY = graphics.DpiY;

        //get true screen size
        var w = (int)Math.Round(screen.Bounds.Width * systemDPI / dpiX);
        var h = (int)Math.Round(screen.Bounds.Height * systemDPI / dpiY);

        return new Size(w, h);
      }
    }

    public static bool MyMakeScreenshot(string folder_to)
    {
      Bitmap bmp_out;
      // сколько мониторов?
      int mon_count = Screen.AllScreens.Count();
      if (mon_count == 1)
      {
        // реальные размеры в пикселях
        var size = GetTrueScreenSize(Screen.PrimaryScreen);

        //create bitmap and make screenshot
        bmp_out = new Bitmap(size.Width, size.Height);
        using (var graph = Graphics.FromImage(bmp_out)) graph.CopyFromScreen(0, 0, 0, 0, size);
      }
      else// 2 моника
      {
        // Primary монитор пусть будет слева
        int m1 = Screen.AllScreens[0].Primary ? 0 : 1;
        int m2 = m1==0 ? 1 : 0;
        // ширина обоих, высота большего
        var s1 = GetTrueScreenSize(Screen.AllScreens[m1]);
        var s2 = GetTrueScreenSize(Screen.AllScreens[m2]);
        int w = s1.Width + s2.Width;
        int h = s1.Height > s2.Height ? s1.Height : s2.Height;
        bmp_out = new Bitmap(w, h);
        Bitmap bmp1 = new Bitmap(s1.Width, s1.Height);
        Bitmap bmp2 = new Bitmap(s2.Width, s2.Height); 
        using (var graph = Graphics.FromImage(bmp1)) graph.CopyFromScreen(0, 0, 0, 0, s1, CopyPixelOperation.SourceCopy);
        using (var graph = Graphics.FromImage(bmp2)) graph.CopyFromScreen(Screen.AllScreens[m2].Bounds.X, Screen.AllScreens[m2].Bounds.Y, 0, 0, s2, CopyPixelOperation.SourceCopy);
        using (var g = Graphics.FromImage(bmp_out)){
          g.DrawImage(bmp1, 0, 0);
          g.DrawImage(bmp2, bmp1.Width, 0);
        }
      }
      //save
      string file_name = DateTime.Now.ToString("yyyy.MM.dd HH.mm.ss") + ".png";
      //bmp.Save(tmp_folder + file_name, ImageFormat.Png);
      bmp_out.Save(folder_to + file_name, ImageFormat.Png);
      bmp_out.Dispose();
      //System.IO.File.Move(tmp_folder + file_name, folder_to + file_name);
      return true;
    }

  }
}
