﻿using System;
using System.Text;
using System.Net.NetworkInformation;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static SmuHelper.Excel;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data;

namespace IndexFiles
{
  class Program
  {

    public static List<String> MyGetOneCol(string sQuery)
    {
      List<String> ret = new List<String>();
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand com = new SqlCommand(sQuery, con))
          {
            using (SqlDataReader r = com.ExecuteReader())
            {
              while (r.Read()) ret.Add(r.GetValue(0).ToString());
            }
            return ret;
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message + "\n" + sQuery);
        return null;
      }
    }

    static void Main(string[] args)
    {

      //Import1s();
      //return;

      if (args.Length == 0){
        TechLog ("No command line args set. Execution enterrupted.");
      }
      else
      {
        foreach (string arg in args)
        {
          if (arg == "/index" || arg == "index" || arg == "/i" || arg == "i") ReindexDirectory();
          if (arg == "/1s" || arg == "1s") Import1s();
        }
      }

      return;
    }

    static void Import1s()
    {
      TechLog("Import 1S data start.");
      string path = MyGetOneValue("select EOValue from _engOptions where EOName='1SImportFolder'")?.ToString() ?? "";
      if (path == "") TechLog("Param '1SImportFolder' not fount in _engOptions");
      else
      {
        if (!path.EndsWith("\\")) path += "\\";
        Regex rgx1 = new Regex(@"^\d{4}-\d{2}\.xlsx$");
        Regex rgx2 = new Regex(@"^\d{4}-\d{2}-\d{2}\.xlsx$");
        Regex rgx3 = new Regex(@"^\d{4}-\d{2}-\d{2}_\d{4}-\d{2}-\d{2}\.xlsx$");
        foreach (string s in Directory.EnumerateFiles(path, "*.xlsx"))
        {
          DateTime dtForm, dtTo;
          string filehort = s.Split(Path.DirectorySeparatorChar).Last();
          if (rgx1.IsMatch(filehort)) {
            /*try
            {*/
              dtForm = new DateTime(int.Parse(filehort.Substring(0, 4)), int.Parse(filehort.Substring(5, 2)), 1);
              dtTo = new DateTime(dtForm.Year, dtForm.Month, DateTime.DaysInMonth(dtForm.Year, dtForm.Month));
              Import1SFileFast(s,dtForm, dtTo);
            /*}
            catch (Exception ex) { TechLog("Cannot load file " + filehort + ": " + ex.Message); }*/
          }
          else if (rgx2.IsMatch(filehort))
          {
            try
            {
              dtForm = new DateTime(int.Parse(filehort.Substring(0, 4)), int.Parse(filehort.Substring(5, 2)), int.Parse(filehort.Substring(8, 2)));
              Import1SFileFast(s,dtForm, dtForm);
            }
            catch (Exception ex) { TechLog("Cannot load file " + filehort + ": " + ex.Message); }
          }
          else if (rgx3.IsMatch(filehort))
          {
            try
            {
              dtForm = new DateTime(int.Parse(filehort.Substring(0, 4)), int.Parse(filehort.Substring(5, 2)), int.Parse(filehort.Substring(8, 2)));
              dtTo = new DateTime(int.Parse(filehort.Substring(11, 4)), int.Parse(filehort.Substring(16, 2)), int.Parse(filehort.Substring(19, 2)));
              Import1SFileFast(s,dtForm, dtTo);
            }
            catch (Exception ex) { TechLog("Cannot load file " + filehort + ": " + ex.Message); }
          }
        }
      }
      TechLog("Import 1S data done.");
    }

    static void MyAddValToQuery(ref string q, dynamic oCell, string datetype)
    {
      bool b;
      string s = oCell.Value?.ToString() ?? "";
      switch (datetype)
      {
        case "string":
          q += (s==""? "null" : MyES(s));
          break;
        case "N0":
          long rl;
          b = long.TryParse(s, out rl);
          if (b) q += "N'" + rl + "'";
          else q += "null";
          break;
        case "N2":
        case "N4":
          decimal rd;
          b = decimal.TryParse(s, out rd);
          if (b) q += "N'" + rd.ToString().Replace(',','.') + "'";
          else q += "null";
          break;
        case "dt":
          DateTime dt = DateTime.MinValue;
          b = DateTime.TryParse(s, out dt);
          if (b) q += "N'" + dt.ToString() + "'";
          else q += "null";
          break;
         default:
          throw new Exception();
      }
      q += ",";
      return;
    }


    static void Import1SFileFast(string sFullFileName, DateTime dtFrom, DateTime dtTo)
    {
      Console.SetCursorPosition(0, 0);
      Console.Write(new String(' ', Console.BufferWidth));
      Console.SetCursorPosition(0, 0);
      Console.Write("Файл: " + sFullFileName);
      Console.SetCursorPosition(0, 1);
      Console.Write(new String(' ', Console.BufferWidth));

      string sFileShortName = sFullFileName.Split(Path.DirectorySeparatorChar).Last();
      TechLog("Загружаем: " + sFileShortName);

      LoadExcelSheetToDB(sFullFileName, dtFrom, dtTo);

      TechLog("Файл обработан: " + sFileShortName);
      string sFileTo = sFullFileName.Substring(0, sFullFileName.Length - sFileShortName.Length) + "loaded\\[" + DateTime.Now.ToString().Replace(':', '.') + "] " + sFileShortName;
      File.Move(sFullFileName, sFileTo);
      //string sFileShortName = sFullFileName.Split(Path.DirectorySeparatorChar).Last();

      return;
      

      /*
      // Open the document for editing.
      using (SpreadsheetDocument document = SpreadsheetDocument.Open(sFullFileName, true))
      {
        IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == sheetName);
        if (sheets.Count() == 0)
        {
          // The specified worksheet does not exist.
          return;
        }
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(relationshipId);

        // Get the cell at the specified column and row.
        Cell cell = GetSpreadsheetCell(worksheetPart.Worksheet, colName, rowIndex);
        if (cell == null)
        {
          // The specified cell does not exist.
          return;
        }
        cell.Remove();
        worksheetPart.Worksheet.Save();
      }

      */







      dynamic oExcel;
      dynamic oSheet;
      bool bNoError = MyExcelOpenFile(sFullFileName, out oExcel, out oSheet);
      if (!bNoError) return;

      oExcel.Visible = false;
      oExcel.ScreenUpdating = false;
      oExcel.DisplayAlerts = false;

      //oSheet.Rows("1:3").Delete();// xlUp
      //oSheet.Columns("A:A").UnMerge();
      //oSheet.Columns("A:D").Delete();
      string q = "";
      int r = 4 /* строк в шапке */, i = 0, rLoaded = 0; ;
      string s = " ";
      bool bHasRows = false;

      q = "delete from Order1S where datediff(d, " + MyES(dtFrom) + ", O1S6)>=0 and datediff(d, O1S6, " + MyES(dtTo) + ")>=0";
      MyExecute(q);

      q = "insert into Order1S (O1S5,O1S6,O1S7,O1S8,O1S9,O1S10,O1S11,O1S12,O1S13,O1S14,O1S15,O1S16,\n" +
        " O1S17,O1S18,O1S19,O1S20,O1S21,O1S22,O1S23,O1S24,O1S25,O1S26,O1S27,O1S28,O1S29,O1S30,O1S31,\n" +
        " O1S32,O1S33,O1S34,O1S35,O1S36,O1S37,O1S38,O1S39,O1S40,O1S41,O1S42,O1S43,O1S44,O1S45,O1S46,\n" +
        " O1S47,O1S48,O1S49,O1S50) values ";

      s = oSheet.Cells(++r, 5).Value?.ToString() ?? ""; //конец файла?
      while (s != "")
      {
        //Загружаем?
        s = oSheet.Cells(r, 48).Value?.ToString() ?? "";
        if (s != "")
        {
          rLoaded++;
          bHasRows = true;
          q += "\n(";
          MyAddValToQuery(ref q, oSheet.Cells(r, 5), "N0");
          MyAddValToQuery(ref q, oSheet.Cells(r, 6), "dt");
          for (i = 7; i <= 15; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 16), "N0");
          for (i = 17; i <= 21; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 22), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 23), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 24), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 25), "dt");
          for (i = 26; i <= 30; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 31), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 32), "N2");
          for (i = 33; i <= 34; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 35), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 32), "N0");
          for (i = 37; i <= 39; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 40), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 41), "N2");
          MyAddValToQuery(ref q, oSheet.Cells(r, 42), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 43), "N0");
          MyAddValToQuery(ref q, oSheet.Cells(r, 44), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 45), "N0");
          for (i = 46; i <= 48; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 49), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 50), "N2");
          q = q.Substring(0, q.Length - 1) + "),";

          Console.SetCursorPosition(0, 1);
          Console.Write("Строка: " + rLoaded + " (" + r + ")");
          //oExcel.Visible = false;
          if (rLoaded % 250 == 0)
          {
            q = q.Substring(0, q.Length - 1) + ";";
            MyExecute(q);
            TechLog("Загружено строк: " + rLoaded);
            q = "insert into Order1S (O1S5,O1S6,O1S7,O1S8,O1S9,O1S10,O1S11,O1S12,O1S13,O1S14,O1S15,O1S16,\n" +
            " O1S17,O1S18,O1S19,O1S20,O1S21,O1S22,O1S23,O1S24,O1S25,O1S26,O1S27,O1S28,O1S29,O1S30,O1S31,\n" +
            " O1S32,O1S33,O1S34,O1S35,O1S36,O1S37,O1S38,O1S39,O1S40,O1S41,O1S42,O1S43,O1S44,O1S45,O1S46,\n" +
            " O1S47,O1S48,O1S49,O1S50) values ";
          }
        }

        /*
          O1SId bigint
          O1S5 bigint
          O1S6 datetime
          O1S7 nvarchar(MAX)
          O1S8 nvarchar(MAX)
          O1S9 nvarchar(MAX)
          O1S10 nvarchar(MAX)
          O1S11 nvarchar(MAX)
          O1S12 nvarchar(MAX)
          O1S13 nvarchar(MAX)
          O1S14 nvarchar(MAX)
          O1S15 nvarchar(MAX)
          O1S16 numeric(18, 0)
          O1S17 nvarchar(MAX)
          O1S18 nvarchar(MAX)
          O1S19 nvarchar(MAX)
          O1S20 nvarchar(MAX)
          O1S21 nvarchar(MAX)
          O1S22 numeric(18, 4)
          O1S23 datetime
          O1S24 numeric(18, 4)
          O1S25 datetime
          O1S26 nvarchar(MAX)
          O1S27 nvarchar(MAX)
          O1S28 nvarchar(MAX)
          O1S29 nvarchar(MAX)
          O1S30 nvarchar(MAX)
          O1S31 datetime
          O1S32 numeric(18, 2)
          O1S33 nvarchar(MAX)
          O1S34 nvarchar(MAX)
          O1S35 datetime
          O1S36 int
          O1S37 nvarchar(MAX)
          O1S38 nvarchar(MAX)
          O1S39 nvarchar(MAX)
          O1S40 numeric(18, 4)
          O1S41 numeric(18, 2)
          O1S42 nvarchar(MAX)
          O1S43 numeric(18, 0)
          O1S44 datetime
          O1S45 numeric(18, 0)
          O1S46 nvarchar(MAX)
          O1S47 nvarchar(MAX)
          O1S48 nvarchar(MAX)
          O1S49 numeric(18, 4)
          O1S50 numeric(18, 2)
        */
        s = oSheet.Cells(++r, 5).Value?.ToString() ?? ""; //конец файла?
      }

      if (rLoaded % 250 != 0)
      {
        q = q.Substring(0, q.Length - 1) + ";";
        MyExecute(q);
        TechLog("Загружено строк: " + rLoaded);
      }

      //oExcel.Visible = true;
      oExcel.ScreenUpdating = true;
      oExcel.DisplayAlerts = true;

      oSheet = null;
      oExcel.Quit();
      TechLog("Файл обработан: " + sFileShortName);
      sFileTo = sFullFileName.Substring(0, sFullFileName.Length - sFileShortName.Length) + "loaded\\[" + DateTime.Now.ToString().Replace(':', '.') + "] " + sFileShortName;
      File.Move(sFullFileName, sFileTo);
      //string sFileShortName = sFullFileName.Split(Path.DirectorySeparatorChar).Last();
      return;
    }


    static void Import1SFile(string sFullFileName, DateTime dtFrom, DateTime dtTo)
    {
      Console.SetCursorPosition(0, 0);
      Console.Write(new String(' ', Console.BufferWidth));
      Console.SetCursorPosition(0, 0);
      Console.Write("Файл: " + sFullFileName);
      Console.SetCursorPosition(0, 1);
      Console.Write(new String(' ', Console.BufferWidth));

      string sFileShortName = sFullFileName.Split(Path.DirectorySeparatorChar).Last();
      TechLog("Загружаем: " + sFileShortName);
      dynamic oExcel;
      dynamic oSheet;
      bool bNoError = MyExcelOpenFile(sFullFileName, out oExcel, out oSheet);
      if (!bNoError) return;

      oExcel.Visible = false;
      oExcel.ScreenUpdating = false;
      oExcel.DisplayAlerts = false;

      //oSheet.Rows("1:3").Delete();// xlUp
      //oSheet.Columns("A:A").UnMerge();
      //oSheet.Columns("A:D").Delete();
      string q = "";
      int r = 4 /* строк в шапке */, i = 0, rLoaded = 0; ;
      string s = " ";
      bool bHasRows = false;

      q = "delete from Order1S where datediff(d, "+MyES(dtFrom)+ ", O1S6)>=0 and datediff(d, O1S6, " + MyES(dtTo) + ")>=0";
      MyExecute(q);

      q = "insert into Order1S (O1S5,O1S6,O1S7,O1S8,O1S9,O1S10,O1S11,O1S12,O1S13,O1S14,O1S15,O1S16,\n" +
        " O1S17,O1S18,O1S19,O1S20,O1S21,O1S22,O1S23,O1S24,O1S25,O1S26,O1S27,O1S28,O1S29,O1S30,O1S31,\n" +
        " O1S32,O1S33,O1S34,O1S35,O1S36,O1S37,O1S38,O1S39,O1S40,O1S41,O1S42,O1S43,O1S44,O1S45,O1S46,\n" +
        " O1S47,O1S48,O1S49,O1S50) values ";

      s = oSheet.Cells(++r, 5).Value?.ToString() ?? ""; //конец файла?
      while (s != "")
      {
        //Загружаем?
        s = oSheet.Cells(r, 48).Value?.ToString() ?? "";
        if (s != "")
        {
          rLoaded++;
          bHasRows = true;
          q += "\n(";
          MyAddValToQuery(ref q, oSheet.Cells(r, 5), "N0");
          MyAddValToQuery(ref q, oSheet.Cells(r, 6), "dt");
          for (i = 7; i <= 15; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 16), "N0");
          for (i = 17; i <= 21; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 22), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 23), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 24), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 25), "dt");
          for (i = 26; i <= 30; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 31), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 32), "N2");
          for (i = 33; i <= 34; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 35), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 32), "N0");
          for (i = 37; i <= 39; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 40), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 41), "N2");
          MyAddValToQuery(ref q, oSheet.Cells(r, 42), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 43), "N0");
          MyAddValToQuery(ref q, oSheet.Cells(r, 44), "dt");
          MyAddValToQuery(ref q, oSheet.Cells(r, 45), "N0");
          for (i = 46; i <= 48; i++) MyAddValToQuery(ref q, oSheet.Cells(r, i), "string");
          MyAddValToQuery(ref q, oSheet.Cells(r, 49), "N4");
          MyAddValToQuery(ref q, oSheet.Cells(r, 50), "N2");
          q = q.Substring(0, q.Length - 1) + "),";

          Console.SetCursorPosition(0, 1);
          Console.Write("Строка: " + rLoaded + " (" + r + ")");
          //oExcel.Visible = false;
          if(rLoaded % 250 == 0)
          {
            q = q.Substring(0, q.Length - 1) + ";";
            MyExecute(q);
            TechLog("Загружено строк: " + rLoaded);
            q = "insert into Order1S (O1S5,O1S6,O1S7,O1S8,O1S9,O1S10,O1S11,O1S12,O1S13,O1S14,O1S15,O1S16,\n" +
            " O1S17,O1S18,O1S19,O1S20,O1S21,O1S22,O1S23,O1S24,O1S25,O1S26,O1S27,O1S28,O1S29,O1S30,O1S31,\n" +
            " O1S32,O1S33,O1S34,O1S35,O1S36,O1S37,O1S38,O1S39,O1S40,O1S41,O1S42,O1S43,O1S44,O1S45,O1S46,\n" +
            " O1S47,O1S48,O1S49,O1S50) values ";
          }
        }

          /*
            O1SId bigint
            O1S5 bigint
            O1S6 datetime
            O1S7 nvarchar(MAX)
            O1S8 nvarchar(MAX)
            O1S9 nvarchar(MAX)
            O1S10 nvarchar(MAX)
            O1S11 nvarchar(MAX)
            O1S12 nvarchar(MAX)
            O1S13 nvarchar(MAX)
            O1S14 nvarchar(MAX)
            O1S15 nvarchar(MAX)
            O1S16 numeric(18, 0)
            O1S17 nvarchar(MAX)
            O1S18 nvarchar(MAX)
            O1S19 nvarchar(MAX)
            O1S20 nvarchar(MAX)
            O1S21 nvarchar(MAX)
            O1S22 numeric(18, 4)
            O1S23 datetime
            O1S24 numeric(18, 4)
            O1S25 datetime
            O1S26 nvarchar(MAX)
            O1S27 nvarchar(MAX)
            O1S28 nvarchar(MAX)
            O1S29 nvarchar(MAX)
            O1S30 nvarchar(MAX)
            O1S31 datetime
            O1S32 numeric(18, 2)
            O1S33 nvarchar(MAX)
            O1S34 nvarchar(MAX)
            O1S35 datetime
            O1S36 int
            O1S37 nvarchar(MAX)
            O1S38 nvarchar(MAX)
            O1S39 nvarchar(MAX)
            O1S40 numeric(18, 4)
            O1S41 numeric(18, 2)
            O1S42 nvarchar(MAX)
            O1S43 numeric(18, 0)
            O1S44 datetime
            O1S45 numeric(18, 0)
            O1S46 nvarchar(MAX)
            O1S47 nvarchar(MAX)
            O1S48 nvarchar(MAX)
            O1S49 numeric(18, 4)
            O1S50 numeric(18, 2)
          */
        s = oSheet.Cells(++r, 5).Value?.ToString() ?? ""; //конец файла?
      }

      if (rLoaded % 250 != 0)
      {
        q = q.Substring(0, q.Length - 1) + ";";
        MyExecute(q);
        TechLog("Загружено строк: " + rLoaded);
      }

      //oExcel.Visible = true;
      oExcel.ScreenUpdating = true;
      oExcel.DisplayAlerts = true;

      oSheet = null;
      oExcel.Quit();
      TechLog("Файл обработан: " + sFileShortName);
      string sFileTo = sFullFileName.Substring(0, sFullFileName.Length - sFileShortName.Length) + "loaded\\["+  DateTime.Now.ToString().Replace(':','.') + "] " + sFileShortName;
      File.Move(sFullFileName, sFileTo);
      //string sFileShortName = sFullFileName.Split(Path.DirectorySeparatorChar).Last();
      return;
    }

    static void ReindexDirectory()
    {
      TechLog("Reindex directories start.");
      MyExecute("delete from _engDirectoryIndex");
      MyExecute("update _engDirectoryToIndex set EDTIName=EDTIName+'\\' where Right(EDTIName,1)<>'\\'");

      /*List<string> ff = new List<string>();
      ff.Add(@"O:\07 – Отдел логистики и комплектации\Протоколы разделения");//MyGetOneCol("select EDTIName from _engDirectoryToIndex;");
      */
      List<string> ff = MyGetOneCol("select EDTIName from _engDirectoryToIndex;");
      foreach (string d in ff)
      {
        //List<string> dd = new List<string>();
        MyGetDirs(/*dd,*/ d, 1);
      }
      TechLog("Reindex directories done.");
    }

    static void MyGetDirs(/*List<string> dd,*/ string d, int iType /* 1 for проекты, 2 for сметы*/)
    {
      string sErr = "";
      string sLastLeaf = d.Substring(0,d.Length-1).Split(Path.DirectorySeparatorChar).Last();
      MyExecute("insert into _engDirectoryIndex (EDIDir,EDILastLeaf,EDIIsFile) values(" + MyES(d) + "," + MyES(sLastLeaf) + ",0)");
      Console.Clear();
      Console.WriteLine("Загрузка из " + d + ":");
      Console.Write("[0]");
      MyGetFiles(d);
      try
      {
        sErr = "Directory.Exists: " + d;
        if (Directory.Exists(d))
        {
          sErr = "Directory.EnumerateDirectories: " + d;
          foreach (string sFolder in Directory.EnumerateDirectories(d, "*", SearchOption.AllDirectories))
          {
            sLastLeaf = sFolder.Split(Path.DirectorySeparatorChar).Last();
            MyExecute("insert into _engDirectoryIndex (EDIDir,EDILastLeaf,EDIIsFile) values(" + MyES(sFolder) + "," + MyES(sLastLeaf) + ",0)");
            Console.SetCursorPosition(0, 1);
            Console.Write(new String(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, 1);
            Console.Write("dir: " + sLastLeaf);
            List<string> sFiles = new List<string>();
            sErr = "Directory.EnumerateFiles: " + d;
            MyGetFiles(sFolder);
          }
        }
      }
      catch(Exception ex)
      {
        TechLog(sErr + "  ||  " + ex.Message);
      }
    }

    static void MyGetFiles(string path)
    {
      string sErr;
      sErr = "Directory.EnumerateFiles: " + path;
      try
      {      
        foreach (string s in Directory.EnumerateFiles(path))
        {
          sErr = "Directory.EnumerateFiles: " + s;
          string sLastLeaf = s.Split(Path.DirectorySeparatorChar).Last();
          if (!sLastLeaf.StartsWith("~$."))
          {
            MyExecute("insert into _engDirectoryIndex (EDIDir,EDILastLeaf,EDIIsFile) values(" + MyES(s) + "," + MyES(sLastLeaf) + ",1)");
            Console.SetCursorPosition(0, 2);
            Console.Write(new String(' ', Console.BufferWidth * 3));
            Console.SetCursorPosition(0, 2);
            Console.Write("file: " + sLastLeaf);
          }
        }
      }
      catch (Exception ex)
      {
        TechLog(sErr + "  ||  " + ex.Message);
      }
    }

  }
}
